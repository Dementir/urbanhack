package db

import (
	"github.com/globalsign/mgo"
	"sync"
	"time"
	"urbanhack/logger"
)

type col struct {
	Name    string
	Indexes []mgo.Index
}

var (
	dbName     string
	mgoSession *mgo.Session
	cols       []*col
	mgoOnce    sync.Once
	inited     = false
)

func PrepareIndexes(collection string, indexes []mgo.Index) {
	if inited {
		logger.L.Error("Prepare index after init is useless.")
	}

	cols = append(cols, &col{
		Name:    collection,
		Indexes: indexes,
	})
}

func ensureIndex(name string, index mgo.Index) {
	s, d := GetMongoInstance()
	err := d.C(name).EnsureIndex(index)
	if err != nil {
		logger.L.Error("Index err: %+v collection: %+v", err, name)
	}
	s.Close()
}

func ensureIndexes() {
	for _, c := range cols {
		for _, i := range c.Indexes {
			go ensureIndex(c.Name, i)
		}
	}
}

func connectMongoDb() *mgo.Session {
	inited = true

	dialInfo, err := mgo.ParseURL("mongodb://192.168.99.100:32782/urbanhack")
	if err != nil {
		logger.L.Critical("Parse url error: %+v", err)
	}

	dbName = dialInfo.Database

	dialInfo.FailFast = false
	dialInfo.Timeout = 10 * time.Second

	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		logger.L.Error(err)
		return nil
	}
	session.SetMode(mgo.PrimaryPreferred, true)

	ticker := time.NewTicker(time.Millisecond * 500)
	go func() {
		for range ticker.C {
			if err := session.Ping(); err != nil {
				logger.L.Error("Mongodb error. Reconnect", err)
				session.Refresh()
			}
		}
	}()

	go ensureIndexes()

	return session
}

func GetMongoInstance() (*mgo.Session, *mgo.Database) {
	mgoOnce.Do(func() {
		mgoSession = connectMongoDb()
	})
	s := mgoSession.Copy()

	return s, s.DB(dbName)
}
