import axios from 'axios'
const config = require('../../config.json')

const state = {
    items: [],
    users: [],
    usersRating: []
}

const getters = {
    getCurrentItemsByTitle: state => id => {
        return state.items.filter(page => page.topic === id)
    }
}

const mutations = {
    changeArray (state, items) {
        state.items = items
    },
    changeUser (state, users) {
        var usersArray = []
        for (var i = 0; i < users.length; i++) {
            var by = users[i]

            by['value'] = false
            usersArray.push(users[i])
        }
        state.users = usersArray
    },
    changeUserRating (state, items) {
        state.usersRating = items
    }
}

const actions = {
    async getEvent ({ commit }) {
        const url = config.backEndUrl + '/getEvents'
        const response = await axios.get(url)
        commit('changeArray', response.data)
    },
    async getUser ({commit}, eventTitle) {
        const url = config.backEndUrl + '/getEvent?eventTitle=' + eventTitle
        axios.get(url).then(function (res) {
            commit('changeUser', res.data.users)
        })
    },
    async getRating ({commit}, eventTitle) {
        const url = config.backEndUrl + '/getRating'
        axios.get(url).then(function (res) {
            commit('changeUserRating', res.data)
        })
    },
    async updateEvent ({commit}, event) {
        const url = config.backEndUrl + '/updateEvent'
        axios.post(url, event).then(function (res) {
            commit('changeArray', res.data)
        })
    },
    async addEvent ({commit}, event) {
        const url = config.backEndUrl + '/addEventFront'
        axios.post(url, event).then(function (res) {
            commit('changeArray', res.data)
        })
    },
    async rating ({commit}, event) {
        const url = config.backEndUrl + '/rating'
        axios.post(url, event).then(function (res) {

        })
    },
    async deleteEvent ({ commit }, eventTitle) {
        const url = config.backEndUrl + '/deleteEvent?eventTitle=' + eventTitle
        axios.delete(url).then(function (res) {
            commit('changeArray', res.data)
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
