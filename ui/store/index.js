import Vue from 'vue'
import Vuex from 'vuex'
import event from './module/event'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = () => {
    return new Vuex.Store({
        modules: {
            event
        },
        strict: debug
    })
}

export default store
