package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo/bson"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"net/http"
	"urbanhack/db"
)

type user struct {
	UserId        int    `json:"user_id"`
	Username      string `json:"username"`
	UserFirstName string `json:"user_first_name"`
	UserLastName  string `json:"user_last_name"`
}

type userReiting struct {
	UserId        int     `json:"user_id"`
	Username      string  `json:"username"`
	UserFirstName string  `json:"user_first_name"`
	UserLastName  string  `json:"user_last_name"`
	Rating        float64 `json:"rating"`
	RaitingPoint  []int   `json:"-"`
}

type eventJson struct {
	EventTitle string `json:"event_title"`
	User       user   `json:"user"`
}

type EventInfo struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Date        string `json:"date"`
	Contact     string `json:"contact"`
	Organizers  string `json:"organizers"`
	Geo         string `json:"geo"`
	Users       []user `json:"users"bson:"user"`
}

func AddUser(ctx *gin.Context) {
	user := new(tgbotapi.User)
	err := ctx.BindJSON(user)
	if err != nil {

	}
	s, d := db.GetMongoInstance()
	defer s.Close()
	d.C("user").Upsert(bson.M{"id": user.ID}, user)
}

func AddUserToEvent(ctx *gin.Context) {
	user := new(eventJson)
	err := ctx.BindJSON(user)
	if err != nil {
		fmt.Println(err.Error())
	}
	s, d := db.GetMongoInstance()
	defer s.Close()
	event := new(EventInfo)
	err = d.C("event").Find(bson.M{"title": user.EventTitle}).One(event)
	if err != nil {
		fmt.Println(err)
	}

	err = d.C("event").Update(bson.M{"title": user.EventTitle}, bson.M{"$push": bson.M{"user": user.User}})

	userRating := new(userReiting)
	userRating.Username = user.User.Username
	userRating.UserId = user.User.UserId
	userRating.UserFirstName = user.User.UserFirstName
	userRating.UserLastName = user.User.UserLastName
	userRating.Rating = 0.0
	err = d.C("user").Insert(userRating)
	if err != nil {
		fmt.Println(err)
	}
}

func AddEvent(ctx *gin.Context) {
	event := new(EventInfo)
	err := ctx.BindJSON(event)
	if err != nil {
		fmt.Println(err.Error())
	}
	s, d := db.GetMongoInstance()
	defer s.Close()
	d.C("event").Insert(event)

}

func AddEventFront(ctx *gin.Context) {
	event := new(EventInfo)
	err := ctx.BindJSON(event)
	if err != nil {
		fmt.Println(err.Error())
	}
	s, d := db.GetMongoInstance()
	defer s.Close()
	d.C("event").Insert(event)
	fmt.Println("AddEventFront")

	ctx.JSON(http.StatusOK, getEvents())
}

func GetEvent(ctx *gin.Context) {
	var title = ctx.Query("eventTitle")
	s, d := db.GetMongoInstance()
	defer s.Close()
	event := new(EventInfo)
	err := d.C("event").Find(bson.M{"title": title}).One(event)
	if err != nil {
		fmt.Println(err)
	}
	ctx.JSON(http.StatusOK, event)

}

func UpdateEvent(ctx *gin.Context) {
	event := new(EventInfo)
	err := ctx.BindJSON(event)
	if err != nil {
		fmt.Println(err.Error())
	}
	s, d := db.GetMongoInstance()
	defer s.Close()

	err = d.C("event").Update(bson.M{"title": event.Title}, bson.M{"$set": bson.M{"description": event.Description, "date": event.Date, "contact": event.Contact, "organizers": event.Organizers}})

	ctx.JSON(http.StatusOK, getEvents())

}

func Rating(ctx *gin.Context) {
	users := new([]userReiting)
	err := ctx.BindJSON(users)
	if err != nil {
		fmt.Println(err.Error())
	}
	s, d := db.GetMongoInstance()
	defer s.Close()
	userRatings := new([]userReiting)
	err = d.C("user").Find(bson.M{}).All(userRatings)

	for _, user := range *userRatings {
		var ratingsUp float64
		var ratingsDown float64
		for _, rating := range user.RaitingPoint {
			ratingsUp = ratingsUp + (float64((rating * rating)))
			ratingsDown = ratingsUp + (float64(rating))
		}
		var by float64
		if ratingsDown == 0 {
			by = 1.0
		} else {
			by = ratingsUp / (ratingsDown * ratingsDown)
		}

		err = d.C("user").Update(bson.M{"userid": user.UserId}, bson.M{"$push": bson.M{"raitingpoint": 1}})
		err = d.C("user").Update(bson.M{"userid": user.UserId}, bson.M{"$set": bson.M{"rating": by}})

		if err != nil {
			fmt.Println(err)
		}
	}
	ctx.Status(http.StatusOK)
	//
	//events := new([]EventInfo)
	//err = d.C("event").Find(bson.M{}).All(events)
	//if err != nil {
	//	fmt.Println(err)
	//}

	//	ctx.JSON(http.StatusOK, events)

}

func GetRating(ctx *gin.Context) {
	s, d := db.GetMongoInstance()
	defer s.Close()
	event := new([]userReiting)
	err := d.C("user").Find(bson.M{}).All(event)

	if err != nil {
		fmt.Println(err)
	}
	ctx.JSON(http.StatusOK, event)

}

func getEvents() []EventInfo {
	s, d := db.GetMongoInstance()
	defer s.Close()
	event := new([]EventInfo)
	err := d.C("event").Find(bson.M{}).All(event)
	if err != nil {
		fmt.Println(err)
	}
	return *event
}

func GetEvents(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, getEvents())
}

func DeleteEvent(ctx *gin.Context) {
	var title = ctx.Query("eventTitle")
	s, d := db.GetMongoInstance()
	defer s.Close()
	err := d.C("event").Remove(bson.M{"title": title})
	if err != nil {
		fmt.Println(err)
	}

	events := getEvents()
	ctx.JSON(http.StatusOK, events)
}

func main() {
	server := gin.Default()
	server.Use(cors.Default())
	server.POST("/addUser", AddUser)
	server.POST("/addEvent", AddEvent)
	server.POST("/addEventFront", AddEventFront)
	server.POST("/addUserToEvent", AddUserToEvent)
	server.POST("/updateEvent", UpdateEvent)
	server.GET("/getEvent", GetEvent)
	server.GET("/getEvents", GetEvents)
	server.GET("/getRating", GetRating)
	server.DELETE("/deleteEvent", DeleteEvent)
	server.POST("/rating", Rating)
	server.Run(":10000")
}
