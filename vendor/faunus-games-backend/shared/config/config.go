package config

import (
	"faunus-games-backend/shared/config/table"
	"faunus-games-backend/shared/logger"
	"github.com/spf13/viper"
	"os"
	"path"
	"runtime"
)

const (
	EnvProd = "prod"
	EnvTest = "test"
)

var (
	configPath   = os.Getenv("TR_CONFIG_PATH")
	Env          = os.Getenv("TR_ENV")
	IsProduction = Env == EnvProd
	BaseUrl      = ""
)

func Get(key string) interface{} {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.Get(key)
}

func GetBool(key string) bool {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetBool(key)
}

func GetStringMapString(key string) map[string]string {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetStringMapString(key)
}

func GetStringMapArray(key string) (map[string][]string, []string) {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	r := viper.GetStringMapStringSlice(key)
	a := []string{}
	for k := range r {
		a = append(a, r[k]...)
	}
	return r, a
}

func GetInt(key string) int {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetInt(key)
}

func GetString(key string) string {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetString(key)
}

func GetFloat(key string) float64 {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetFloat64(key)
}

func GetStringSlice(key string) []string {
	if !IsSet(key) {
		logger.L.Fatalf("Key \"%s\" not found in config file", key)
	}
	return viper.GetStringSlice(key)
}

var IsSet = viper.IsSet

func readFile(filePath string) {
	viper.SetConfigFile(filePath)
	if err := viper.ReadInConfig(); err != nil {
		logger.L.Fatalf("Fatal error config file: %s", err)
	}
}

func mergeFile(filePath string) {
	viper.SetConfigFile(filePath)
	viper.MergeInConfig()
}

func parseConfig() {
	if len(configPath) == 0 {
		_, filename, _, _ := runtime.Caller(1)
		dir := path.Dir(filename)
		configPath = path.Join(dir, "../")
	}

	configName := "config.dev.json"
	if IsProduction {
		configName = "config.prod.json"
	}

	readFile(path.Join(configPath, configName))

	switch Env {
	case EnvTest:
		mergeFile(path.Join(configPath, "config.test.json"))
	default:
		mergeFile(path.Join(configPath, "config.local.json"))
	}
}

func init() {
	parseConfig()
	table.Init()

	BaseUrl = GetString("baseUrl")

	table.RowString("ENV", Env)
	table.RowString("Config path", configPath)

}
