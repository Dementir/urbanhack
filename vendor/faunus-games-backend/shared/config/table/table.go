package table

import (
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"
)

const padding = 3

var w *tabwriter.Writer

func Init() {
	w = tabwriter.NewWriter(os.Stdout, 0, 0, padding, ' ', 0)
}

func RowString(name string, value string) {
	fmt.Fprintln(w, name+"\t"+value)
}

func RowInt(name string, value int) {
	fmt.Fprintln(w, name+"\t"+strconv.Itoa(value))
}

func Print() {
	w.Flush()
}
