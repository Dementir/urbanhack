package logger

import (
	"github.com/op/go-logging"
	"os"
)

var L = logging.MustGetLogger("")

func init() {
	format := `%{color}%{time:15:04:05.000} %{level} %{shortfile} ▶ %{color:reset}%{message}`

	backend := logging.NewLogBackend(os.Stderr, "", 0)
	formatter := logging.NewBackendFormatter(backend, logging.MustStringFormatter(format))

	backendLeveled := logging.AddModuleLevel(formatter)
	backendLeveled.SetLevel(logging.DEBUG, "")
	if os.Getenv("TR_ENV") == "prod" {
		backendLeveled.SetLevel(logging.INFO, "")
	} else {
		backendLeveled.SetLevel(logging.DEBUG, "")
	}

	logging.SetBackend(backendLeveled)
}

