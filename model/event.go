package model

import (
	"github.com/globalsign/mgo"
	"urbanhack/db"
)

func init() {
	db.PrepareIndexes("events", []mgo.Index{
		{
			Key:        []string{"title"},
			Unique:     true,
			DropDups:   false,
			Background: true,
			Sparse:     false,
		},
	})
}
